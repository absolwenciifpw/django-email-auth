__author__ = 'jb'

from django.contrib.auth.forms import AuthenticationForm
from django import forms
from django.utils.translation import ugettext_lazy as _

class EmailAuthForm(AuthenticationForm):

    username = forms.EmailField(
        label=_("Email"), max_length=30,
        help_text=_("Your email for which you registered yout account"))
